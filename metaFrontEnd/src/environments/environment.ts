// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyCGoB_kLNC3QpC5keyNwdxUoUOWpCTsRJY",
    authDomain: "pockmeta.firebaseapp.com",
    databaseURL: "https://pockmeta.firebaseio.com",
    projectId: "pockmeta",
    storageBucket: "pockmeta.appspot.com",
    messagingSenderId: "564715502604",
    appId: "1:564715502604:web:81765696218d8290210229",
    measurementId: "G-9V5VTKNPFT"

  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
