import { DataService } from './services/data.service';
import { CentralRxJsService } from './services/centralRXJS.service';
import { HomeComponent } from './pages/home/home.component';
import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { AppRoutingModule } from './app.routing';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
// AngularFire
import { AngularFireModule } from '@angular/fire';
import { AngularFireDatabaseModule } from '@angular/fire/database';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { AngularFireMessagingModule } from '@angular/fire/messaging';
import { AngularFireStorageModule } from '@angular/fire/storage';
import { AngularFirePerformanceModule } from '@angular/fire/performance';

import { environment } from '../environments/environment';
// Modules
import { HttpClientModule } from '@angular/common/http';
/* import { PagesModule } from './pages/page.module'; */
import { MaterialModule } from './material.module';
import { ComponentsModule } from './components/components.module';


const AngularFire = [
  AngularFireModule.initializeApp(environment.firebase),
  AngularFireDatabaseModule,
  AngularFireAuthModule,
  AngularFireMessagingModule,
  AngularFireStorageModule,
  AngularFirePerformanceModule
];
const FormModules = [ReactiveFormsModule, FormsModule];


@NgModule({
  declarations: [
    AppComponent,
    HomeComponent
  ],
  imports: [
    HttpClientModule,
    AppRoutingModule,
    MaterialModule,
    BrowserModule,
    BrowserAnimationsModule,
    ...FormModules,
    ComponentsModule,
    // AngularFire
    ...AngularFire,
  ],
  providers: [
    CentralRxJsService,
    DataService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
