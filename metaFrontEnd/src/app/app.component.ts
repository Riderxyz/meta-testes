import { MatIconRegistry } from '@angular/material/icon';
import { CentralRxJsService } from 'src/app/services/centralRXJS.service';
import { Component, OnInit } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { config } from './services/config';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'metaFrontEnd';
  activatedTheme: string = 'purpleish-theme';
  constructor(
    private centralRXJS: CentralRxJsService,
    private matIconRegistry: MatIconRegistry,
    private domSanitizer: DomSanitizer
  ) {
   /*  this.matIconRegistry.addSvgIcon(
      "graalLogo",
      this.domSanitizer.bypassSecurityTrustResourceUrl("../assets/icons/logos/graal-Logo.svg")
    ); */
  }
  ngOnInit() {
    this.centralRXJS.DataToReceive.subscribe((res) => {
      if (res.key === config.rxjsCentralKeys.changeTheme) {
        console.log('858',res);

        this.activatedTheme = res.data;
      }
    })
   // moment.locale('pt-br');

  }
}
