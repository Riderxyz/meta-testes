import { Component, OnInit, ViewChild } from '@angular/core';
import { Contato } from './../../models/contato.interface';
import { DataService } from './../../services/data.service';
import { MatSelectionList } from '@angular/material/list';
import {
  MatDialog,
  MatDialogRef,
  MAT_DIALOG_DATA,
} from '@angular/material/dialog';

import { DeleteDialogComponent } from './../../components/delete-dialog/delete-dialog.component';
import { EditDialogComponent } from './../../components/edit-dialog/edit-dialog.component';
import {
  zoomInOnEnterAnimation,
  hingeOnLeaveAnimation,
} from 'angular-animations';
@Component({
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
  animations: [zoomInOnEnterAnimation(), hingeOnLeaveAnimation()],
})
export class HomeComponent implements OnInit {
  contatoArr: Contato[] = [];
  displayArr: Contato[] = [];
  test = null;
  @ViewChild('contatos') contatoViewChildList: MatSelectionList;
  constructor(private dataSrv: DataService, private editDialog: MatDialog) {}

  ngOnInit(): void {
    this.dataSrv.getContatos.subscribe((res: Contato[]) => {
      console.log('Entrei na linhga 14');
      res.forEach((element) => {
        element.selected = false;
      });
      this.contatoArr = res;
    });
  }

  addContatos() {
    const dialogRef = this.editDialog.open(EditDialogComponent, {
      width: '500px',
    });
    dialogRef.afterClosed().subscribe((result) => {
      console.log('The dialog was closed', result);
    });
  }
  editContatos(event: Contato) {
    console.log(event);
    const dialogRef = this.editDialog.open(EditDialogComponent, {
      width: '500px',
      data: event,
    });
    dialogRef.afterClosed().subscribe((result) => {
      console.log('The dialog was closed', result);
    });
  }
  openDeleteSheet() {
    const contatoDel = [];
    this.contatoViewChildList.selectedOptions.selected.forEach((element) => {
      contatoDel.push(element.value);
    });
    console.log(contatoDel);
    if (contatoDel.length !== 0) {
      console.log('nada');
      const dialogRef = this.editDialog.open(DeleteDialogComponent, {
     width: '250px',
     data: contatoDel,
   });
 dialogRef.afterClosed().subscribe((result) => {
     console.log('The dialog was closed', result);
   });
    }
  }
}
