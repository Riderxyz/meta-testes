import { FormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HeaderComponent } from './header/header.component';
import { MaterialModule } from '../material.module';
import {DeleteDialogComponent } from './delete-dialog/delete-dialog.component';
import { EditDialogComponent } from './edit-dialog/edit-dialog.component';
import { LoaderComponent } from './loader/loader.component';
@NgModule({
  declarations: [
    HeaderComponent,
    DeleteDialogComponent,
    EditDialogComponent,
    LoaderComponent,
  ],
  imports: [CommonModule, MaterialModule, FormsModule],
  exports: [HeaderComponent, MaterialModule, LoaderComponent],
  providers: [],
})
export class ComponentsModule {}
