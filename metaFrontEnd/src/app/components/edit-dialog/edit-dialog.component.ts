import { DataService } from './../../services/data.service';
import { Component, OnInit, Inject, AfterViewInit } from '@angular/core';
import {
  MatDialog,
  MatDialogRef,
  MAT_DIALOG_DATA,
} from '@angular/material/dialog';
import { Contato } from 'src/app/models/contato.interface';
@Component({
  templateUrl: './edit-dialog.component.html',
  styleUrls: ['./edit-dialog.component.scss'],
})
export class EditDialogComponent implements OnInit, AfterViewInit {
  EditMode = false;
  title = 'Adicionando Contato';
  userData: Contato = {
    nome: null,
    telefone: null,
    email: null,
    obs: null,
    id: ' ',
  };
  constructor(
    public dialogRef: MatDialogRef<EditDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: Contato,
    private dataSrv: DataService
  ) {}

  ngOnInit(): void {
    console.log(this.data);
    if (this.data !== undefined && this.data !== null) {
      this.title = 'Editando ' + this.data.nome;
      this.EditMode = true;
      this.userData = this.data;
    }
  }
  ngAfterViewInit() {

  }
  deny() {

  }

  save() {
    if (this.EditMode) {
      this.dataSrv.editContatos = this.userData;
      this.dialogRef.close();
    } else {
      this.userData.id = '_' + Math.random().toString(36).substr(2, 9);
      this.dataSrv.addContatos = this.userData;
      this.dialogRef.close();
    }
  }
}
