import { DataService } from '../../services/data.service';
import { Component, OnInit, Inject } from '@angular/core';
import {
  MatDialog,
  MatDialogRef,
  MAT_DIALOG_DATA,
} from '@angular/material/dialog';
@Component({
  templateUrl: './delete-dialog.component.html',
  styleUrls: ['./delete-dialog.component.scss'],
})
export class DeleteDialogComponent implements OnInit {
  constructor(
    public dialogRef: MatDialogRef<DeleteDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private dataSrv: DataService
  ) {}

  ngOnInit(): void {}

  confirm() {
    this.dataSrv.deleteContato = this.data;
    this.dialogRef.close();
    }
  deny() {
    this.dialogRef.close((el) => {
    });
  }
}
