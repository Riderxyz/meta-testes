import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AngularFireDatabase } from '@angular/fire/database';
import { Contato } from '../models/contato.interface';
@Injectable({ providedIn: 'root' })
export class DataService {
  constructor(
    private httpClient: HttpClient,
    private db: AngularFireDatabase
  ) {}

  get getContatos() {
    return this.db.list('/contatosPublicos').valueChanges();
  }

  set addContatos(contato: Contato) {
    this.db
    .list('/contatosPublicos')
    .set(contato.id, contato).then(()=> {

    })
  }
  set editContatos(contato: Contato) {
    this.db
    .list('/contatosPublicos')
    .set(contato.id, contato).then(()=> {

    })
  }
  set deleteContato(arrContato: Contato[]) {
    console.log(arrContato);
    console.log('Dentro do Delete');

    for (let i = 0; i < arrContato.length; i++) {
      const element = arrContato[i];
      this.db.object('/contatosPublicos/' + element.id).remove();
    }
  }
}
