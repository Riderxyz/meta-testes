import restify from "restify";
import admin from "firebase-admin";
import { Contato } from "./contato.interface";

export const editContato = async (
  _req: restify.Request,
  _res: restify.Response,
  _next: restify.Next
) => {
  const body: Contato = _req.body;
  admin
    .database()
    .ref(`/contatosPublicos`)
    .child(body.id)
    .set(body)
    .then(() => {
      _res.send({
        status: 200,
        msg: "Os dados foram editados com sucesso",
      });
    })
    .catch((err) => {
      _res.send({
        status: 500,
        msg: "Os dados não foram salvos. Tente novamente mais tarde",
        err,
      });
    });
};
