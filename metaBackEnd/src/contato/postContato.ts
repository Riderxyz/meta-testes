import restify from "restify";
import admin from "firebase-admin";
import { Contato } from "./contato.interface";

export const addContato = async (
  _req: restify.Request,
  _res: restify.Response,
  _next: restify.Next
) => {
  const body: Contato = _req.body;
  body.id = "_" + Math.random().toString(36).substr(2, 9);
  admin
    .database()
    .ref(`/contatosPublicos`)
    .child(body.id)
    .set(body)
    .then(() => {
      _res.send({
        status: 200,
        msg: "Os dados foram salvos com sucesso",
      });
    })
    .catch((err) => {
      _res.send({
        status: 500,
        msg: "Os dados não foram salvos. Tente novamente mais tarde",
        err,
      });
    });
};
