import restify from "restify";
import admin from "firebase-admin";
import axios from "axios";
import { Contato } from "./contato/contato.interface";

 
export const populateDataBase = (
  _req: restify.Request,
  _res: restify.Response,
  _next: restify.Next
): void => {
  axios
    .get("https://my.api.mockaroo.com/pockmeta.json?key=a5e40240")
    .then((contatos) => {
      contatos.data.forEach(async (element:Contato, index: number) => {
          element.id = '_' + Math.random().toString(36).substr(2, 9);
          await admin.database().ref(`/contatosPublicos`).remove();
          await admin.database().ref(`/contatosPublicos`).child(element.id).set(element);
          if (index === contatos.data.length - 1) {
            console.log("acabei");
            _res.send('Acabei. Banco de dados resetado');
          }
        }); 
    })
    .catch((err) => {
      _res.send(err);
      _next();
    });
};
