import { getContato } from './contato/getContato';
import { addContato } from './contato/postContato';
import { editContato } from './contato/putContato';
import { deleteContato } from './contato/deleteContato';
import { populateDataBase } from './populateDataBase';
import restify from 'restify';
import admin from 'firebase-admin';
export const server = restify.createServer({
    name: 'myAPI',
    version: '1.0.0'
});

var serviceAccount = require("./serviceAccountKey.json");
admin.initializeApp({
    credential: admin.credential.cert(serviceAccount),
    databaseURL: "https://pockmeta.firebaseio.com"
  });

//parsing settings
server.use(restify.plugins.acceptParser(server.acceptable));
server.use(restify.plugins.queryParser());
server.use(restify.plugins.bodyParser());
server.use((_req,res,next) => {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "X-Requested-With");
    return next();
})/* crossOrigin(req,res,next)()=> {

}) */
server.pre(restify.plugins.pre.sanitizePath());

server.post('/resetar', (req, res, next) => {
    populateDataBase(req, res, next);
});

server.get('/getContato', ((req, res, next) => {
    getContato(req, res, next);
}));

server.post('/addContato', ((req, res, next) => {
    addContato(req, res, next);
}));
server.put('/editContato', ((req, res, next) => {
    editContato(req, res, next);
}));
server.del('/deleteContato', ((req, res, next) => {
    deleteContato(req, res, next);
}));

//call the routes.ts file for available REST API routes
console.log('setting routes...');

//when running the app will listen locally to port 51234
server.listen(4004, function() {
    console.log('%s listening at %s', server.name, server.url);
})